<?php

namespace Drupal\system_nodes;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\node\NodeInterface;
use Drupal\system_nodes\Entity\NodeRoleInterface;

class SystemNodeStorage implements SystemNodeStorageInterface {

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $storage;

  /**
   * @var integer[]
   */
  protected $cache = [];

  /**
   * @var string[][]
   */
  protected $index;

  /**
   * SystemNodeStorage constructor.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   */
  public function __construct(KeyValueFactoryInterface $keyValueFactory) {
    $this->storage = $keyValueFactory->get('system_nodes');
  }

  /**
   * {@inheritdoc}
   */
  public function lookup(NodeRoleInterface $role) {
    $id = $role->id();
    if (!isset($this->cache[$id])) {
      $this->cache[$id] = $this->storage->get($id);
    }
    return $this->cache[$id];
  }

  /**
   * {@inheritdoc}
   */
  public function reverseLookup(NodeInterface $node) {
    $index = $this->getIndex();
    return isset($index[$node->id()]) ? $index[$node->id()] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function set(NodeInterface $node, array $roleIds) {
    $nid = (int) $node->id();
    $roleIds = array_combine($roleIds, $roleIds);

    foreach ($roleIds as $roleId) {
      $this->cache[$roleId] = $nid;
      $this->storage->set($roleId, $nid);
    }

    $this->index = $this->getIndex();
    foreach ($this->index as $otherNode => $otherRoles) {
      $this->index[$otherNode] = array_diff_key($otherRoles, $roleIds);
    }
    $this->index[$nid] = $roleIds;
    foreach ($this->index as $otherNode => $otherRoles) {
      if (!$otherRoles) {
        unset($this->index[$otherNode]);
      }
    }
    $this->storage->set('index', $this->index);
  }

  /**
   * @return string[][]
   */
  protected function getIndex() {
    if ($this->index === NULL) {
      $this->index = $this->storage->get('index', []);
    }
    return $this->index;
  }

}
