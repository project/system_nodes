<?php

namespace Drupal\system_nodes\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to delete Node role entities.
 */
class NodeRoleDeleteForm extends EntityDeleteForm {

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $storage;

  /**
   * NodeRoleDeleteForm constructor.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $storage
   */
  public function __construct(KeyValueFactoryInterface $storage) {
    // The role assignments are stored as content, not configuration.
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('keyvalue'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->storage->get('system_nodes')->delete($this->getEntity()->id());
  }

}
