<?php

namespace Drupal\system_nodes\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class NodeRoleForm.
 *
 * @package Drupal\system_nodes\Form
 */
class NodeRoleForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $node_role = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $node_role->label(),
      '#description' => $this->t('Label for the content role.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $node_role->id(),
      '#machine_name' => [
        'exists' => '\Drupal\system_nodes\Entity\NodeRole::load',
      ],
      '#disabled' => !$node_role->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $node_role = $this->entity;
    $status = $node_role->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label node role.', [
          '%label' => $node_role->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label node role.', [
          '%label' => $node_role->label(),
        ]));
    }
    $form_state->setRedirectUrl($node_role->toUrl('collection'));
  }

}
