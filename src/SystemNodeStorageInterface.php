<?php

namespace Drupal\system_nodes;

use Drupal\node\NodeInterface;
use Drupal\system_nodes\Entity\NodeRoleInterface;

interface SystemNodeStorageInterface {

  /**
   * @param \Drupal\system_nodes\Entity\NodeRoleInterface $role
   *
   * @return int
   */
  public function lookup(NodeRoleInterface $role);

  /**
   * @param \Drupal\node\NodeInterface $node
   *
   * @return string[]
   */
  public function reverseLookup(NodeInterface $node);

  /**
   * @param \Drupal\node\NodeInterface $node
   * @param array                      $roleIds
   *
   * @return mixed
   */
  public function set(NodeInterface $node, array $roleIds);

}
