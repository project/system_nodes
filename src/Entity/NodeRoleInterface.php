<?php

namespace Drupal\system_nodes\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Node role entities.
 */
interface NodeRoleInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
