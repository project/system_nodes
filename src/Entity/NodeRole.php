<?php

namespace Drupal\system_nodes\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Node role entity.
 *
 * @ConfigEntityType(
 *   id = "node_role",
 *   label = @Translation("Node Role"),
 *   handlers = {
 *     "list_builder" = "Drupal\system_nodes\NodeRoleListBuilder",
 *     "form" = {
 *       "add" = "Drupal\system_nodes\Form\NodeRoleForm",
 *       "edit" = "Drupal\system_nodes\Form\NodeRoleForm",
 *       "delete" = "Drupal\system_nodes\Form\NodeRoleDeleteForm"
 *     }
 *   },
 *   config_prefix = "role",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/system-nodes/{node_role}",
 *     "add-form" = "/admin/config/system-node/add",
 *     "edit-form" = "/admin/config/system-node/{node_role}/edit",
 *     "delete-form" = "/admin/config/system-node/{node_role}/delete",
 *     "collection" = "/admin/config/system-node"
 *   }
 * )
 */
class NodeRole extends ConfigEntityBase implements NodeRoleInterface {

  /**
   * The Node role ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Node role label.
   *
   * @var string
   */
  protected $label;

}
