<?php

namespace Drupal\system_nodes\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\node\Controller\NodeViewController;
use Drupal\node\Entity\Node;
use Drupal\system\Controller\Http4xxController;
use Drupal\system_nodes\Entity\NodeRoleInterface;
use Drupal\system_nodes\SystemNodeStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SystemNodeController extends ControllerBase {

  /**
   * @var \Drupal\system_nodes\SystemNodeStorageInterface
   */
  protected $storage;

  /**
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $path;

  /**
   * @var \Drupal\node\Controller\NodeViewController
   */
  protected $controller;

  /**
   * @var \Drupal\node\NodeInterface[]
   */
  protected $cache = [];
  /**
   * @var \Drupal\system\Controller\Http4xxController
   */
  private $fallback;

  /**
   * SystemNodeController constructor.
   *
   * @param \Drupal\system_nodes\SystemNodeStorageInterface $storage
   * @param \Drupal\Core\Path\CurrentPathStack              $path
   * @param \Drupal\node\Controller\NodeViewController      $controller
   * @param \Drupal\system\Controller\Http4xxController     $fallback
   */
  public function __construct(SystemNodeStorageInterface $storage,
                              CurrentPathStack $path,
                              NodeViewController $controller,
                              Http4xxController $fallback) {
    $this->storage = $storage;
    $this->controller = $controller;
    $this->path = $path;
    $this->fallback = $fallback;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('system_nodes.storage'),
      $container->get('path.current'),
      NodeViewController::create($container),
      Http4xxController::create($container)
    );
  }

  /**
   * @param \Drupal\system_nodes\Entity\NodeRoleInterface $node_role
   *
   * @return array
   */
  public function view(NodeRoleInterface $node_role) {
    // The system-node/* path should be valid regardless of content.
    // Therefore we can't throw HTTP 4xx errors here.
    try {
      $node = $this->resolveRole($node_role);
      // Forward the request to the node/{node} route.
      // The path must be altered for the theme layer to work correctly.
      $this->path->setPath('node/' . $node->id());
      return $this->controller->view($node);
    }
    // Note that on error pages, the fallback page shows the error encountered
    // on accessing the node, not the original error.
    // This should not happen if the site is configured correctly,
    // and is only useful for debugging purposes.
    catch (AccessDeniedHttpException $exception) {
      return $this->fallback->on403();
    }
    catch (NotFoundHttpException $exception) {
      return $this->fallback->on404();
    }
  }

  /**
   * @param \Drupal\system_nodes\Entity\NodeRoleInterface $node_role
   *
   * @return string
   */
  public function title(NodeRoleInterface $node_role) {
    try {
      $node = $this->resolveRole($node_role);
      // Forward the request to the node/{node} route.
      // The path must be altered for the theme layer to work correctly.
      $this->path->setPath('node/' . $node->id());
      return $this->controller->title($node);
    }
    catch (AccessDeniedHttpException $exception) {
      return $this->t('Access denied');
    }
    catch (NotFoundHttpException $exception) {
      return $this->t('Page not found');
    }
  }

  /**
   * @param \Drupal\system_nodes\Entity\NodeRoleInterface $role
   *
   * @return \Drupal\node\NodeInterface
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  protected function resolveRole(NodeRoleInterface $role) {
    if (!isset($this->cache[$role->id()])) {
      /** @var \Drupal\node\NodeInterface $node */
      $nid = $this->storage->lookup($role);
      $this->cache[$role->id()] = $nid ? Node::load($nid) : NULL;
    }

    if (!$node = $this->cache[$role->id()]) {
      throw new NotFoundHttpException();
    }
    if (!$node->access('view')) {
      throw new AccessDeniedHttpException();
    }
    return $node;
  }

}
