<?php

namespace Drupal\system_nodes;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Node role entities.
 */
class NodeRoleListBuilder extends ConfigEntityListBuilder {

  /**
   * @var \Drupal\system_nodes\SystemNodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * NodeRoleListBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface         $entity_type
   * @param \Drupal\Core\Entity\EntityStorageInterface      $storage
   * @param \Drupal\system_nodes\SystemNodeStorageInterface $nodeStorage
   */
  public function __construct(EntityTypeInterface $entity_type,
                              EntityStorageInterface $storage,
                              SystemNodeStorageInterface $nodeStorage) {
    parent::__construct($entity_type, $storage);
    $this->nodeStorage = $nodeStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container,
                                        EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('system_nodes.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['path'] = $this->t('Path');
    $header['node'] = $this->t('Assigned content');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['path']['data']['#markup'] = '<code>/system-node/' . $entity->id() . '</code>';
    $nid = $this->nodeStorage->lookup($entity);
    if ($nid && $node = Node::load($nid)) {
      $row['node'] = $node->toLink();
    }
    else {
      $row['node'] = $this->t('<em>Not assigned</em>');
    }
    return $row + parent::buildRow($entity);
  }

}
