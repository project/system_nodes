# Introduction

This module provides an intermediary between configured system paths
and created content.

System paths (such as the site's front page and error pages) can refer
to content such as node pages. These may not be available in all
environments, and may be deleted by content editors who have no access
to update the site configuration.

As a solution, System Nodes provides configurable "soft reference" paths
that can be used in the configuration, and which can be assigned to nodes
while editing.

This allows different environments to use the same configuration value
without directly referencing content in the configuration.
